var matrixExample = [
   [ 1, 2, 3, 4 ],
   [ 4, 5, 6, 5 ],
   [ 7, 8, 9, 7 ],
   [ 7, 8, 9, 7 ]
];
function sumUpDiagonals(matrix) {
  var result = [0,0];
  var ml = matrix.length;
  for (var i = 0; i < ml; i++) {
    result[0] += matrix[i][i];
    result[1] += matrix[i][ml - i - 1];
  }
  return result;
}
console.log(sumUpDiagonals(matrixExample))
