import React from 'react';
import { Route, IndexRoute }  from 'react-router';
import App from 'components/App';
import AuthPage from 'components/AuthPage';
import AddPage from 'components/AddPage';
import MainPage from 'components/MainPage';
import ErrorPage from 'components/ErrorPage';

export default (
  <Route component={App} path='/'>
    <IndexRoute component={MainPage} />
    <Route component={AuthPage} path='auth' />
    <Route component={AddPage} path='add' />
    <Route component={ErrorPage} path='*' />
  </Route>
);
