import React, { Component } from 'react';
import Nav from '../Nav';
import axios from 'axios';

import './MainPage.styl';

class MainPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      query: '',
      onlyImportant: false,
      loading: true,
      tasks: []
    }

    this.removeTask = this.removeTask.bind(this);
    this.handleChangeSearch = this.handleChangeSearch.bind(this);
    this.handleChangeOnlyImportant = this.handleChangeOnlyImportant.bind(this);
  }

  componentDidMount() {
    const _this = this;
    axios.post('/api/getAll').then((res) => {
      _this.setState({loading: false});
      if (res.data && res.data.tasks) {
        const timeNow = new Date();
        const tasksView = res.data.tasks.map((task) => {
          let curTask = task;
          const passTime = timeNow - Date.parse(task.createDate);

          if (passTime < 1000*60) {
            curTask.passTime = parseInt(passTime/1000) + ' сек';
          } else if (passTime < 1000*60*60) {
            curTask.passTime = parseInt(passTime/1000/60) + ' мин';
          } else if (passTime < 1000*60*60*24) {
            curTask.passTime = parseInt(passTime/1000/60/60) + ' час';
          } else {
            curTask.passTime = parseInt(passTime/1000/60/60/24) + ' дн';
          }
          return curTask;
        });
        _this.setState({tasks: tasksView});
      }
    }).catch(function (err) {
      _this.setState({loading: false});
    });
  }

  removeTask(taskId) {
    const _this = this;
    axios.post('/api/removeTask', {
      taskId: taskId
    }).then((res) => {
      if (!res.data.error) {
        let taskNumberRemove = -1;
        let curTasks = _this.state.tasks;
        for (let i = 0; i < curTasks.length; i++) {
          if (taskId === curTasks[i]._id.toString()) {
            taskNumberRemove = i;
          }
        }
        if (taskNumberRemove !== -1) {
          curTasks.splice(taskNumberRemove, 1);
        }
        _this.setState({tasks: curTasks});
      }
    }).catch(function (err) {
      _this.setState({loading: false});
    });
  }

  handleChangeSearch(e) {
    this.setState({query: e.target.value});
  }

  handleChangeOnlyImportant() {
    this.setState({onlyImportant: !this.state.onlyImportant});
  }

  render() {
    const onlyImportant = this.state.onlyImportant;
    const query = this.state.query.toLowerCase();

    const filterTask = this.state.tasks.filter((task)=>{
      const text = (task.title + ' ' + task.description).toLowerCase();
      if (onlyImportant && !task.isImportant) return false;
      return text.indexOf(query) !== -1;
    }).reverse();

    const tasks = filterTask.map((task, index) => {
      return (
        <div className="col-xs-12 col-sm-6 col-md-3" key={index}>
          <div className="task">
            <div className="task__close" onClick={()=>{this.removeTask(task._id)}}></div>
            <div className="task__title">
              {task.title}
              {
                (task.isImportant) ? <span className="label label-danger">Важно</span> : ''
              }

            </div>
            <div className="task__description">
              {task.description}
            </div>
            <div className="task__description">
              Добавлена {task.passTime} назад
            </div>
          </div>
        </div>
      );
    });

    return (
      <div className="container">
        <Nav/>
        <div className="row">
          <div className="col-xs-12">
            <input type="text" placeholder="Поиск"
              name="query" className="form-control"
              value={this.state.query} onChange={this.handleChangeSearch} />
            <div className="checkbox">
              <label>
                <input type="checkbox" checked={this.state.onlyImportant}
                  onChange={this.handleChangeOnlyImportant}/> Только важные
              </label>
            </div>
          </div>
        </div>
          {
            (this.state.loading) ?
              <h1>Загружаем</h1>
            :
              <div className="row">
                {tasks}
              </div>
          }
        </div>
    );
  }
}

export default MainPage;

