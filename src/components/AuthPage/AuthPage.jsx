import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import axios from 'axios';

class AuthPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      login: '',
      password: '',
      authorizing: false,
      error: ''
    }

    this.handleChangeLogin = this.handleChangeLogin.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChangeLogin(e) {
    this.setState({login: e.target.value});
  }

  handleChangePassword(e) {
    this.setState({password: e.target.value});
  }

  handleSubmit(e) {
    e.preventDefault();
    const _this = this;
    if (_this.state.authorizing) return;
    _this.setState({authorizing: true});

    axios.post('/api/authorize', {
      login: _this.state.login,
      password: _this.state.password
    }).then((res) => {
      _this.setState({authorizing: false});

      if (res.data.error) {
        _this.setState({error: res.data.error});
      } else {
        browserHistory.push('/');
      }
    }).catch(function (err) {
      _this.setState({authorizing: false});
    });
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit} action="/api/authorize" method="POST">
        <fieldset id="fieldset">
          <div className="form-group">
            <label htmlFor="login">Логин</label>
            <input id="login" type="text" placeholder="Логин"
              name="login" className="form-control"
              value={this.state.login} onChange={this.handleChangeLogin} />
          </div>
          <div className="form-group">
            <label htmlFor="password">Пароль</label>
            <input id="password" type="password" placeholder="Пароль"
              name="password" className="form-control"
              value={this.state.password} onChange={this.handleChangePassword} />
          </div>
          <button type="submit" className="btn btn-default">Авторизоваться</button>
          <span className={"label label-warning" + (!this.state.authorizing ? " hide": "")}>Идет авторизация...</span>
          <span className={"label label-danger" + (!this.state.error ? " hide": "")}>{this.state.error}</span>
        </fieldset>
      </form>
    );
  }
}

export default AuthPage;

