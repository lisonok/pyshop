import React, { Component } from 'react';

import './normalize.css';
import './bootstrap.css';

class App extends Component {
  render() {
    return (
      <div className='container'>
        {this.props.children}
      </div>
    );
  }
}

export default App;
