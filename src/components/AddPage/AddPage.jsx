import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import Nav from '../Nav';
import axios from 'axios';

class AddPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: '',
      description: '',
      isImportant: false,
      loading: false,
      error: ''
    }

    this.handleChangeTitle = this.handleChangeTitle.bind(this);
    this.handleChangeDescription = this.handleChangeDescription.bind(this);
    this.handleChangeIsMain = this.handleChangeIsMain.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChangeTitle(e) {
    this.setState({title: e.target.value});
  }

  handleChangeDescription(e) {
    this.setState({description: e.target.value});
  }

  handleChangeIsMain() {
    this.setState({isImportant: !this.state.isImportant});
  }

  handleSubmit(e) {
    e.preventDefault();
    const _this = this;
    if (_this.state.loading) return;
    _this.setState({loading: true});

    axios.post('/api/addTask', {
      title: _this.state.title,
      description: _this.state.description,
      isImportant: _this.state.isImportant
    }).then((res) => {
      _this.setState({loading: false});
      if (res.data.error) {
        _this.setState({error: res.data.error});
      } else {
        browserHistory.push('/');
      }
    }).catch(function (err) {
      _this.setState({loading: false});
    });
  }

  render() {
    return (
      <div className="container">
        <Nav/>
        <form onSubmit={this.handleSubmit} action="/api/addTask" method="POST">
          <fieldset id="fieldset">
            <div className="form-group">
              <label htmlFor="title">Заголовок</label>
              <input id="title" type="text" placeholder="Заголовок"
                name="title" className="form-control"
                value={this.state.title} onChange={this.handleChangeTitle} />
            </div>
            <div className="form-group">
              <label htmlFor="description">Описание</label>
              <textarea id="description" type="text" placeholder="Описание" rows="5"
                name="description" className="form-control"
                onChange={this.handleChangeDescription} value={this.state.description}>
              </textarea>
            </div>
            <div className="checkbox">
              <label>
                <input type="checkbox" checked={this.state.isImportant}
                  onChange={this.handleChangeIsMain}/> Пометить как важное
              </label>
            </div>
            <button type="submit" className="btn btn-default">Добавить заметку</button>

            <span className={"label label-warning" + (!this.state.loading ? " hide": "")}>Добавляем...</span>
            <span className={"label label-danger" + (!this.state.error ? " hide": "")}>{this.state.error}</span>
          </fieldset>
        </form>
      </div>
    );
  }
}

export default AddPage;

