import React, { Component } from 'react';
import { browserHistory, Link } from 'react-router';
import axios from 'axios';

class ErrorPage extends Component {
  constructor(props) {
    super(props);

    this.handleLogout = this.handleLogout.bind(this);
  }

  handleLogout(e) {
    e.preventDefault();
    axios.post('/api/logout').then((res) => {
      if (!res.data.error) {
        browserHistory.push('/auth');
      }
    }).catch(function (err) {
      console.log(err);
    });
  }

  render() {
    return (
      <nav className="navbar navbar-default">
        <div className="container-fluid">
          <div className="navbar-header">
            <ul className="nav navbar-nav">
              <li>
                <Link to='/'>Заметки</Link>
              </li>
              <li>
                <Link to='/add'>Добавить заметку</Link>
              </li>
            </ul>
          </div>
          <ul className="nav navbar-nav navbar-right">
            <ul className="nav navbar-nav">
              <li>
                <Link to='#' onClick={this.handleLogout}>Выход</Link>
              </li>
            </ul>
          </ul>
        </div>
      </nav>
    )
  }
}

export default ErrorPage;

