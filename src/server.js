import express from 'express';
import React from 'react';
import ReactDom from 'react-dom/server';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import { match, RouterContext } from 'react-router';
import routes from './routes';
import assetsUrl from './assets.json';
import apiRouter from './routers/api';
import mongoose from './mongoose.js';

import session from 'express-session';
const MongoStore = require('connect-mongo')(session);

const app = express();

// Сборка файлов
app.use('/public', express.static('public'));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json());

// cookie
app.use(cookieParser());
// session
app.use(session({
  secret: '1234567890QWERTY',
  key: "sid",
  cookie: {
    "path": "/",
    "httpOnly": true,
    "maxAge": null
  },
  store: new MongoStore({mongooseConnection: mongoose.connection}),
  resave: true,
  saveUninitialized: true
}));

// Api
app.use('/api', apiRouter);

app.use((req, res) => {
  match({ routes, location: req.url }, (error, redirectLocation, renderProps) => {
    if (error) {
      return res.status(500).send(error.message);
    } else if (redirectLocation) {
      return res.redirect(301, redirectLocation.pathname + redirectLocation.search);
    } else if (renderProps) {
      if (req.url !== '/auth' && !req.session.user) {
        return res.redirect(301, '/auth');
      }
      const componentHTML = ReactDom.renderToString(
        <RouterContext {...renderProps} />
      );
      return res.end(renderHTML(componentHTML))
    } else {
      return res.status(404).send('Not found');
    }
  })
});

const assetsHost = process.env.NODE_ENV !== 'production' ? 'http://localhost:8050' : '';

function renderHTML(componentHTML) {
  return `
    <!DOCTYPE html>
      <html>
      <head>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Rushop Test Quest</title>
          <link rel="stylesheet" href="${assetsHost}${assetsUrl.bundle.css}">
      </head>
      <body>
        <div id="react-view">${componentHTML}</div>
        <script type="application/javascript" src="${assetsHost}${assetsUrl.bundle.js}"></script>
      </body>
    </html>
  `;
}

const PORT = process.env.PORT || 3001;

app.listen(PORT, () => {
  console.log(`Server listening on: ${PORT}`);
});
