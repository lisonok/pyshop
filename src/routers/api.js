import express from 'express';
import { authorize, addTask, removeTask, getAllTask } from '../services/user';

let router = express.Router();

// Добавить таск
router.post('/getAll', async (req, res) => {
  try {
    if (!req.session.user) throw new Error('Вы не авторизованы');
    const tasks = await getAllTask(req.session.user);
    res.json({
      tasks: tasks
    });
  } catch (e) {
    res.json({
      error: e.toString()
    });
  }
});

// Добавить таск
router.post('/addTask', async (req, res) => {
  try {
    if (!req.session.user) throw new Error('Вы не авторизованы');
    const user = await addTask(
      req.session.user,
      req.body.title,
      req.body.description,
      req.body.isImportant);

    res.json({});
  } catch (e) {
    res.json({
      error: e.toString()
    });
  }
});

// Удалить таск
router.post('/removeTask', async (req, res) => {
  try {
    if (!req.session.user) throw new Error('Вы не авторизованы');
    const user = await removeTask(req.session.user, req.body.taskId);
    res.json({});
  } catch (e) {
    res.json({
      error: e.toString()
    });
  }
});

// Авторизация
router.post('/authorize', async (req, res) => {
  try {
    const user = await authorize(req.body.login, req.body.password);
    req.session.user = user.login;
    res.json({});
  } catch (e) {
    res.json({
      error: e.toString()
    });
  }
});

// Выход
router.post('/logout', async (req, res) => {
  try {
    req.session.destroy();
    res.json({});
  } catch (e) {
    res.json({
      error: e.toString()
    });
  }
});

export default router;
