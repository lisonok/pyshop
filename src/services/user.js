import User from '../models/User';

// Авторизация
export const authorize = async (login, password) => {
  // Не введен логин или пароль
  if (!login || !password) throw new Error("Неверные данные");
  // Ищем пользователя
  const curUser = await User.findOne({login: login});
  if (curUser) {
    // Пользователь найден
    if (curUser.checkPassword(password)) {
      return curUser;
    } else {
      throw new Error("Неверный пароль");
    }
  } else {
    // Пользователь не найден, создаем нового
    let newUser = new User({
      login: login,
      password: password,
      tasks: []
    });
    const curNewUser = await newUser.save();
    return curNewUser;
  }
}

// Поулчить все таски
export const getAllTask = async (login) => {
  if (!login) throw new Error("Неверные данные");

  // Ищем пользователя
  const curUser = await User.findOne({login: login});

  // Если пользотель не найден, то вызываем ошибку
  if (!curUser) throw new Error("Возникла ошибка, текущий пользователь не найден");

  return curUser.tasks;
}


// Добавить таск
export const addTask = async (login, title, description, isImportant=false) => {
  if (!login || !title || !description) throw new Error("Неверные данные");

  // Ищем пользователя
  const curUser = await User.findOne({login: login});

  // Если пользотель не найден, то вызываем ошибку
  if (!curUser) throw new Error("Возникла ошибка, текущий пользователь не найден");

  // Создаем таск
  const task = { title, description, isImportant, createDate: new Date() };

  curUser.tasks.push(task);

  // Сохраняем
  const result = await curUser.save();
  return result;
}

// Удалить таск
export const removeTask = async (login, taskId) => {
  if (!login || !taskId) throw new Error("Неверные данные");

  // Ищем пользователя
  const curUser = await User.findOne({login: login});

  // Если пользотель не найден, то вызываем ошибку
  if (!curUser) throw new Error("Возникла ошибка, текущий пользователь не найден");

  let taskNumberRemove = -1;
  for (let i = 0; i < curUser.tasks.length; i++) {
    if (taskId === curUser.tasks[i]._id.toString()) {
      taskNumberRemove = i;
    }
  }
  if (taskNumberRemove === -1) throw new Error("Не найдена такая заметка");

  curUser.tasks.splice(taskNumberRemove, 1);

  // Сохраняем
  const result = await curUser.save();
  return result;
}
