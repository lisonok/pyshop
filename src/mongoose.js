import mongoose from 'mongoose';

mongoose.connect('mongodb://localhost/ryshop');
mongoose.Promise = global.Promise;

export default mongoose;
