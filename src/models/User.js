import crypto from 'crypto';
import mongoose from '../mongoose.js';
import { Schema } from 'mongoose';

let userSchema = new Schema({
  login: {
    type: String,
    unique: true,
    required: true
  },
  passwordHash: {
    type: String,
    required: true
  },
  salt: {
    type: String,
    required: true
  },
  tasks: [{
    createDate: Date,
    title: String,
    description: String,
    isImportant: Boolean
  }]
});

// Получаем хэш пароля
userSchema.methods.encryptPassword = function (password) {
  const hash = crypto.createHmac('sha1', this.salt)
    .update(password).digest('hex');
  return hash;
};

// Get/Set для password
userSchema.virtual('password')
  .set(function(password) {
    console.log(this, password);
    this._plainPassword = password;
    this.salt = Math.random() + '';
    this.passwordHash = this.encryptPassword(password);
  })
  .get(function() {
    return this._plainPassword;
  });

// Проверка пароля
userSchema.methods.checkPassword = function (password) {
  return this.encryptPassword(password) == this.passwordHash;
}

export default mongoose.model('User', userSchema);

