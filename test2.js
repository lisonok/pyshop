function isEven(number) {
    // Если число не целое то null
    if (Math.round(number) != number) return null;
    // Returns True if **number** is even or False if it is odd.
    return number % 2 === 0;
}

// Test
describe("isEven", function() {

  it("При вещественном числе null", function() {
    assert(isEven(0.05) === null, "isEven(0.05) не null");
  });

  it("0 это четное число", function() {
    assert(isEven(0), "isEven(0) не true");
  });

  it("Любое число кроме 0", function() {
    assert(!isEven(1), "isEven(1) не false");
    assert(isEven(2), "isEven(2) не true");
    assert(isEven(-100), "isEven(100) не true");
    assert(!isEven(111), "isEven(1) не false");
    assert(!isEven(-111), "isEven(1) не false");
    assert(isEven(-20000000), "isEven(20000000) не true");
  });
});
